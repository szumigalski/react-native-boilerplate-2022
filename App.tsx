import React from 'react'
import * as eva from '@eva-design/eva'
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components'
import { EvaIconsPack } from '@ui-kitten/eva-icons'
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import MainView from './components/Main'
import Second from './components/Second'
import { Provider } from 'react-redux'
import store from './store'
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'

const Stack = createNativeStackNavigator()
const persistor = persistStore(store)

export default function App () {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <IconRegistry icons={EvaIconsPack} />
          <ApplicationProvider {...eva} theme={eva.light}>
            <Stack.Navigator initialRouteName='Main'>
              <Stack.Screen name='Main' component={MainView} />
              <Stack.Screen name='Second' component={Second} />
            </Stack.Navigator>
          </ApplicationProvider>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  )
}
