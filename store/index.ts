import { configureStore } from '@reduxjs/toolkit'
import testReducer from './slices/testSlice'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { persistReducer } from 'redux-persist'
import { combineReducers } from 'redux'
import thunk from 'redux-thunk'

export const reducers = combineReducers({
  test: testReducer
})

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whiteList: []
}
const persistedReducer = persistReducer(persistConfig, reducers)

export default configureStore({
  reducer: persistedReducer,
  middleware: [thunk]
})
