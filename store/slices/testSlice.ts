import { createSlice } from '@reduxjs/toolkit'

export const testSlice = createSlice({
  name: 'test',
  initialState: {
    status: 'idle',
    error: null
  },
  reducers: {}

})

// Action creators are generated for each case reducer function
// export const {} = testSlice.actions

export default testSlice.reducer
