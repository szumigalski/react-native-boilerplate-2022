import type { NativeStackNavigationProp } from '@react-navigation/native-stack'

export type HomeStackNavigatorParamList = {
  Main: undefined;
  Second: undefined;
};

export type HomeScreenNavigationProp = NativeStackNavigationProp<
  HomeStackNavigatorParamList,
  'Second'
>;
