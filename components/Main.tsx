import React from 'react'
import { View } from 'react-native'
import { Button, Text, Icon } from '@ui-kitten/components'
import { useNavigation } from '@react-navigation/native'
import { HomeScreenNavigationProp } from '../types'

const MainView = () => {
    const navigation = useNavigation<HomeScreenNavigationProp>()

    return (
      <View style={{ flex: 1, height: '100%', padding: 20 }}>
        <Text category='h2'>Home</Text>
        <Icon
          fill='#8F9BB3'
          style={{ width: 32, height: 32 }}
          name='star'
        />
        <Button onPress={() => navigation.navigate('Second')}>To Second</Button>
      </View>)
}

export default MainView
