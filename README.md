# Getting started
First time install all dependencies by 

- `yarn install`

Next time to run application 

- `react-native run-android`


## Create apk

To create apk tou need
- `cd android`
- `./gradlew assembleRelease` or for Google Play `./gradlew bundleRelease`

## Included libraries

- TypeScript
- **UI Kitten** - to style
- **redux toolkit** with **async storage** -  store of application
- react navigation - moving between views

Application has **eslint** and **prettier**

## CI/CD
Branch have to named `mob-*` and then pipeline creates apk file

## License

This project is released under the [MIT License](LICENSE).